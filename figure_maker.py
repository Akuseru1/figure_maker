import pandas as pd
import matplotlib.pyplot as plt
# from scipy.signal import find_peaks

def make(file_, name="figure"):
    sheet = pd.read_excel(file_, index_col=0)
    
    #formato de datos
    sheet["Fecha"] = sheet["Fecha"].str.split(" ").str[1]
    
    #crea y dibuja figura
    fig, ax = plt.subplots(figsize=(15, 10))
    ax.plot(sheet["Fecha"], sheet["Valor"], color='C1')
    
    # dibuja picos
    # ax.plot(peaks, sheet["Valor"][peaks], "x")
    
    # información figura
    plt.title("Medidor Nivel Canal Parshall", fontweight="bold")
    plt.xlabel("Valores del Medidor", fontweight="bold")
    plt.ylabel("Hora del Dia", fontweight="bold")
    
    # guardar y mostrar
    fig.savefig(name, format='png')
    plt.show()

